
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , api = require('./routes/api')
  , http = require('http')
  , path = require('path')
  , helpers = require('./helpers')
  , passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('nodeBB-xks'));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});


function loadUser(userName, cb){
  helpers.getUserByName(userName, function(err, user){
    if(err) { cb(err, null); }
    else { cb(null, user) }
  });
}

function isLogged(req, res, next){
  if(req.session.logged) {
    next();
  } else {
    res.redirect('/login');
  }
}

function isLoggedJSON(req, res, next){
  if(req.session.logged){
    next();
  } else {
    res.json({ errors: { message: 'not logged in' }});
  }
}

/************************/

app.get('/login', routes.login);
app.post('/login', function(req, res){
  loadUser(req.body['username'], function(err, user){
    if(err) {
      console.log(err);
      res.json({ errors: err });
    } else {
      if(user){
        if(req.body['password'] == user.password){
          req.session.logged = true;
          req.session.userId = user.id;
          req.session.username = user.username;
          req.session.email = user.email;
          req.session.isAdmin = user.isAdmin;
          res.redirect('/');
        } else {
          res.redirect('/login');
        }
      } else {
        res.redirect('/login');
      }
      
    }
  })
});
app.get('/logout', function(req,res){
  req.session.destroy(function(err){
    console.log(err);
    res.redirect('/login');
  });
});

app.get('/', isLogged, routes.index);

app.get('/sync', routes.sync);
app.get('/test', isLogged, function(req, res){
  res.json({ user: req.session });
});

app.get('/api', api.index);
// CRUD USER
app.post('/api/user', api.postUser);
app.get('/api/users', api.getUsers);
app.get('/api/usernames', api.getUsernames);
app.get('/api/user/:id', api.getUser);
app.put('/api/user/:id', api.putUser);
app.del('/api/user/:id', function(req, res){

});
app.get('/api/session', function(req, res){
  if(req.session.logged) res.json(req.session);
  else res.json({ logged: false });
});

// CRUD CATEGORIES
app.post('/api/category', api.postCategory);
app.get('/api/categories', api.getCategories);
app.get('/api/category/:id', api.getCategory);
app.put('/api/category/:id', api.putCategory);

// CRUD DISCUSSIONS
app.post('/api/discussion', api.postDiscussion);
app.get('/api/discussions/:categoryId', api.getDiscussions);
app.get('/api/discussion/:id', api.getDiscussion);

// CRUD COMMENTS
app.post('/api/comment', api.postComment);
app.get('/api/comments/:id', api.getComments);

app.post('/api/activity', api.postActivity);
app.get('/api/activities', api.getActivities);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
