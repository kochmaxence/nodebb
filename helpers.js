var models = require('./models'),
	_ = require('underscore');

var userProfileAttrs = ['id', 'username', 'email', 'isAdmin'];

/** USERS HELPERS **/
exports.getUsers = function(cb){
	models.User.findAll({
		attributes: userProfileAttrs
	}).error(function(error){
		cb(error, null);
	}).success(function(users){
		cb(null, users);
	});
}

exports.GetUsersFull = function(cb){
	models.User.findAll().error(function(error){
		cb(error, null);
	}).success(function(users){
		cb(null, users);
	});
}

exports.getUser = function(userId, cb){
	models.User.find({
		where: { id: userId },
		attributes: userProfileAttrs
	}).error(function(error){
		cb(error, null);
	}).success(function(user){
		cb(null, user);
	});
}

exports.getUserFull = function(userId, cb){
	models.User.find({
		where: { id: userId }
	}).error(function(error){
		cb(error, null);
	}).success(function(user){
		cb(null, user);
	});
}

exports.getUserByName = function(userName, cb){
	models.User.find({
		where: { username: userName }
	}).error(function(error){
		cb(error, null);
	}).success(function(user){
		cb(null, user);
	});
}

exports.postUser = function(profile, cb){
	var user = models.User.build({
		username	: profile.username,
		password	: profile.password,
		email		: profile.email
	});
	user.save().error(function(error){
		cb(error, null);
	}).success(function(iUser){
		cb(null, iUser.values);
	});
}

exports.putUser = function(userId, updates, cb){
	models.User.find({
		where: { id: userId }
	}).error(function(error){
		cb(error, null);
	}).success(function(user){
		user.updateAttributes(updates).error(function(error){
			cb(error, null);
		}).success(function(user){
			cb(null, user);
		});
	});
}

/** CATEGORIES HELPERS **/
exports.getCategories = function(cb){
	models.Category.findAll().error(function(error){
		cb(error, null);
	}).success(function(categories){
		cb(null, categories);
	});
}

exports.getCategory = function(categoryId, cb){
	models.Category.find({
		where: { id: categoryId }
	}).error(function(error){
		cb(error, null);
	}).success(function(category){
		cb(null, category);
	});
}

exports.postCategory = function(profile, cb){
	var category = models.Category.build({
		name		: profile.name,
		description	: profile.description
	});
	category.save().error(function(error){
		cb(error, null);
	}).success(function(iCategory){
		cb(null, iCategory);
	});
}

exports.putCategory = function(categoryId, updates, cb){
	models.Category.find({
		where: { id: categoryId }
	}).error(function(error){
		cb(error, null);
	}).success(function(category){
		category.updateAttributes(updates).error(function(error){
			cb(error, null);
		}).success(function(category){
			cb(null, category);
		});
	});
}

// DISCUSSIONS HELPERS

exports.getDiscussions = function(categoryId, cb){
	models.Discussion.findAll({
		where: { CategoryId: categoryId }
	}).error(function(error){
		cb(error, null);
	}).success(function(discussions){
		cb(null, discussions);
	})
}

exports.getDiscussion = function(discussionId, cb){
	models.Discussion.find({
		where: { id: discussionId }
	}).error(function(error){
		cb(error, null);
	}).success(function(discussion){
		cb(null, discussion);
	});
}

exports.postDiscussion = function(profile, cb){
	var discussion = models.Discussion.build({
		name		: profile.name,
		body		: profile.body,
		announce	: profile.announce,
		CategoryId	: profile.categoryId,
		UserId		: profile.userId
	});
	discussion.save().error(function(error){
		cb(error, null);
	}).success(function(discussion){
		cb(null, discussion);
	});
}

// COMMENTS HELPERS

exports.getComments = function(discussionId, cb){
	models.Discussion.find({
		where	: { id: discussionId }
	}).error(function(error){
		cb(error, null);
		console.log('error');
	}).success(function(discussion){
		if(discussion){
			discussion.getComments().error(function(error){
				cb(error, null);
			}).success(function(comments){
				cb(null, comments, discussion);
			});	
		} else {
			var error = 'Discussion #' + discussionId + ' does not exist.';
			cb(error, null);
		}
	});
}

exports.postComment = function(profile, cb){
	var comment = models.Comment.build({
		UserId			: profile.userId,
		DiscussionId	: profile.discussionId,
		body			: profile.body
	});
	comment.save().error(function(error){
		cb(error, null);
	}).success(function(comment){
		cb(null, comment);
	});
}

// ACTIVITIES HELPERS

exports.getActivities = function(cb){
	models.Activity.findAll({
		limit: 20,
		order: 'id DESC'
	}).error(function(error){
		cb(error, null);
	}).success(function(activities){
		cb(null, activities);
	});
}

exports.postActivity = function(profile, cb){
	var activity = models.Activity.build({
		UserId		:	profile.UserId,
		username	: 	profile.username,
		body		:	profile.body
	});
	activity.save().error(function(error){
		cb(error, null);
	}).success(function(activity){
		cb(null, activity);
	});
}