var Sequelize = require('sequelize'),
	sequelize = new Sequelize('xkbb', 'xks', '##');

var User = sequelize.define('User', {
	id			:	{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },	
	username	:	{ type: Sequelize.STRING, allowNull: false },
	password	:	{ type: Sequelize.TEXT, allowNull: false },
	email		:	{ type: Sequelize.STRING, allowNull: false },
	isAdmin		:	{ type: Sequelize.INTEGER, defaultValue: 0 }
})

var Category = sequelize.define('Category', {
	id			:	{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	name		:	{ type: Sequelize.STRING, allowNull: false },
	description	:	{ type: Sequelize.STRING },
});

var Discussion = sequelize.define('Discussion', {
	id			:	{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	lastCommId	:	{ type: Sequelize.INTEGER },
	name		:	{ type: Sequelize.STRING },
	body		:	{ type: Sequelize.TEXT, allowNull: false },
	announce	:	{ type: Sequelize.INTEGER },
	date		:	{ type: Sequelize.DATE, defaultValue: Sequelize.NOW }
});

var Comment = sequelize.define('Comment', {
	id			:	{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	body		:	{ type: Sequelize.TEXT, allowNull: false },
	date		:	{ type: Sequelize.DATE, defaultValue: Sequelize.NOW }
});

var Activity = sequelize.define('Activity', {
	id			:	{ type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	username	:	{ type: Sequelize.STRING, allowNull: false },
	body		:	{ type: Sequelize.STRING, allowNull: false }
});

Category.hasMany(Discussion);
Discussion.hasMany(Comment);
Comment.belongsTo(Discussion);
User.hasMany(Comment).hasMany(Discussion).hasMany(Activity);

var self = module.exports = {
	db: sequelize,

	User: User,
	Category: Category,
	Discussion: Discussion,
	Comment: Comment,
	Activity: Activity
}