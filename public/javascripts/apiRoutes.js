var routes = {
	apiCreate: {
		user		: '/api/user',
		category	: '/api/category',
		discussion	: '/api/discussion',
		comment		: '/api/comment'
	},
	apiRead: {
		users 		: '/api/users',
		user 		: '/api/user/:id',
		categories 	: '/api/categories',
		category	: '/api/category/:id',
		discussions	: '/api/discussions',
		discussion	: '/api/discussion/:id',
		comments	: '/api/comments/:discussionId',

	},
	apiUpdate: {
		user		: '/api/user/:id',
		category	: '/api/category/:id',
		discussion	: '/api/discussion/:id',
		comment		: '/api/comment/:id'
	},
	apiDelete: {
		user		: '/api/user/:id',
		category	: '/api/category/:id',
		discussion	: '/api/discussion/:id',
		comment		: '/api/comment/:id'
	}
}

exports.routes = routes;