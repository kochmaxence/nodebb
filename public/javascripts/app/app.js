'use strict';

angular.module('nodeBB', ['nodeBB.services']).
	config(['$routeProvider', function($routeProvider){
		$routeProvider.
			when('/categories', {
				templateUrl: 'partials/categories.html',
				controller: CategoriesCtrl
			}).
			when('/discussions/:categoryId', { 
				templateUrl: 'partials/discussions.html', 
				controller: DiscussionsCtrl
			}).
			when('/discussion/:discussionId', {
				templateUrl: 'partials/discussion.html', 
				controller: DiscussionCtrl
			}).
			otherwise({
				redirectTo: '/categories'
			});
	}]);
