'use strict';

function appCtrl($rootScope, $scope, $http){
	$http.get('api/session').success(function(data){
		if(data.logged == true){
			$rootScope.user = data;
		} else {
			$rootScope.user = {
				logged: false
			}
		}
	});
	$http.get('api/usernames').success(function(data){
		$rootScope.users = data;
	});
};

function ActivitiesCtrl($rootScope, $scope, $http, Activities, socket){
	
	$scope.activities = Activities.query();
	$scope.postActivity = function(identifier){
		var data = {
			body		:	$(identifier + ' input[type="text"]').val()
		}
		$http.post('api/activity', data).success(function(data){
			console.log('posted');
		});
		//$scope.activities = Activities.query();

	}
	$scope.orderProp = '-id';

	socket.on('activity:new', function(data){
		$scope.activities.push(data);
	});
}

function NotificationsCtrl($scope, $rootScope, socket){
	$scope.count = 0;
	$scope.lastId = 0;
	$scope.notifications = [];
	$scope.orderProp = '-id';

	function notifyCss(){
		var identifier = $('#notificationCount');
		if($scope.count < 1){
			identifier.removeClass().addClass('badge badge-info');
		} else if($scope.count > 5) {
			identifier.removeClass().addClass('badge badge-important');
		} else if($scope.count > 0) {
			identifier.removeClass().addClass('badge badge-warning');
		} 
	}

	$scope.closeNotification = function($event){
		var identifier;

		if($event.srcElement.nodeName == 'A'){
			identifier = $($event.srcElement);
		} else if($event.srcElement.nodeName == 'STRONG'){
			identifier = $($event.srcElement).parent('a');
		}

		identifier.css('color', '#ccc');
		identifier.removeAttr('ng-click');
		if($scope.count > 0){
			$scope.count = $scope.count - 1;
		}
		notifyCss();
	}
	
	socket.on('comment:new', function(data){
		$scope.lastId = $scope.lastId + 1;
		var alert = {
			id: $scope.lastId + 1,
			title: 'Comment',
			username: $rootScope.users[data.UserId].username,
			target: '/#/discussion/' + data.DiscussionId 
		};

		$scope.notifications.push(alert);
		// notify($rootScope.users[data.UserId].username, 'posted a <a href="/#/discussion/' + data.DiscussionId + '">comment</a>');
		$scope.count = $scope.count + 1;
		notifyCss();
		//$('#activities ul').prepend('<li>New comment by ' + $rootScope.users[data.UserId].username + ' on <a href="/#/discussion/' + data.DiscussionId + '">discussion</a></li>');
	});
	socket.on('discussion:new', function(data){
		$scope.lastId = $scope.lastId + 1;
		var alert = {
			id: $scope.lastId + 1,
			title: 'Discussion',
			username: $rootScope.users[data.UserId].username,
			target: '/#/discussion/' + data.id
		};

		$scope.notifications.push(alert);
		// notify($rootScope.users[data.UserId].username, 'posted a new <a href="/#/discussion/' + data.id + '">discussion</a>');
		$scope.count = $scope.count + 1;
		notifyCss();
		//$('#activities ul').prepend('<li>' + $rootScope.users[data.UserId].username + ' opened <a href="/#/discussion/' + data.id + '">' + data.name + '</a></li>');
	});
}

function LoginCtrl($scope, $http){
	$scope.submitLogin = function(identifier){
		var data = {
			username : $(identifier + ' input[type="text"]').val(),
			password : $(identifier + ' input[type="password"]').val()
		}
		$http.post('/login', data)
			.error(function(err){
			console.log(err);
			})
			.success(function(data){
					console.log('LOGIN');
					console.log(data);
			});
	}
	
}

function CategoriesCtrl($scope, $http, socket){
	$http.get('api/categories').success(function(data){
		console.log(data);
		$scope.categories = data;
	});

	$http.get('api/discussions/1').success(function(data){
		$scope.discussions = data;
	});

	$scope.orderProp = 'id';
	socket.on('category:new', function(data){
		$scope.categories.push(data);
	});
}

function DiscussionsCtrl($rootScope, $scope, $http, $routeParams, socket){
	$scope.categoryId = $routeParams.categoryId;
	$http.get('api/discussions/' + $routeParams.categoryId).success(function(data){
		$scope.discussions = data;
	});

	$scope.newDiscussion = function(){
		$('#newDiscussion').modal();
	}
	$scope.postDiscussion = function(){
		var target = '#newDiscussion';
		var data = {
			name		: $(target + ' input[name="name"]').val(),
			body		: $(target + ' textarea[name="body"]').val(),
			announce	: 0,
			categoryId	: $scope.categoryId
		};
		console.log(data);
		$http.post('api/discussion', data).success(function(data){
			$('#newDiscussion').modal('hide');
		});
	}

	socket.on('discussion:new', function(data){
		if($scope.categoryId == data.CategoryId){
			$scope.discussions.push(data);
		}
	});
}

function DiscussionCtrl($rootScope, $scope, $http, $routeParams, socket){
	$scope.discussionId = $routeParams.discussionId;
	$http.get('api/comments/' + $routeParams.discussionId).success(function(data){
		console.log(data);
		$scope.discussion = data;
	});

	$scope.newComment = function(){
		$('#newComment').modal();
	}
	$scope.postComment = function(){
		console.log($scope.discussionId);
		var target = '#newComment';
		var data = {
			discussionId	: $scope.discussionId,
			body			: $(target + ' textarea[name="body"]').val()
		};
		$http.post('api/comment', data).success(function(data){
			$(target).modal('hide');
		});
	}

	socket.on('comment:new', function(data){
		if($scope.discussionId == data.DiscussionId){
			$scope.discussion.comments.push(data);
		}
	});
}


// Use $inject if minified
//IndexCtrl.$inject = ['$scope', '$http'];