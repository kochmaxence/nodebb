'use strict';

angular.module('nodeBB.services', ['ngResource']).
	factory('Activities', function($resource){
		return $resource('api/activities', {}, {
			query: { 
				method: 'GET',
				isArray: true
			}
		});
	}).
	factory('socket', function($rootScope){
		var socket = io.connect('http://bubook.be:3001');
		return {
			on: function(eventName, callback){
				socket.on(eventName, function(){
					var args = arguments;
					$rootScope.$apply(function(){
						callback.apply(socket, args);
					});
				});
			},
			emit: function(eventName, data, callback){
				socket.emit(eventName, data, function(){
					var args = arguments;
					$rootScope.$apply(function(){
						if(callback){
							callback.apply(socket, args);
						}
					});
				});
			}
		}
	})
