var models = require('../models'),
	helpers = require('../helpers'),
	_ = require('underscore');
	apiRoutes = require('../public/javascripts/apiRoutes'),
	sio = require('../sio');

exports.index = function(req, res){
	res.json(apiRoutes);
}

/**************************/

exports.getUsers = function(req, res){
	helpers.getUsers(function(err, users){
		if(err) res.json({ errors: err });
		else res.json(users);
	})
}

exports.getUsernames = function(req, res){
	helpers.getUsers(function(err, users){
		if(err){ res.json({ errors: err }) }
		else {
			if(users){
				var list = {};
				_.each(users, function(user){
					list[user.id] = { username : user.values.username };
				});
				res.json(list);
			} else {
				res.json({ errors: 'no users' });
			}
		}
	});
}

exports.getUser = function(req, res){
	helpers.getUser(parseInt(req.params.id), function(err, user){
		if(err) res.json({ errors: err });
		else res.json(user);
	});
}

exports.postUser = function(req, res){
	helpers.postUser({
		username	: req.body['username'],
		password	: req.body['password'],
		email		: req.body['email']
	}, function(err, user){
		if(err) res.json({ errors: err });
		else res.json(user);
	})
}

exports.putUser = function(req, res){
	helpers.putUser(req.params.id, {
		username	: req.body['username'],
		password	: req.body['password'],
		email		: req.body['email'],
	}, function(err, user){
		if(err) res.json({ errors: err });
		else res.json(user);
	});
}


/**************************/

exports.getCategories = function(req, res){
	helpers.getCategories(function(err, categories){
		if(err) res.json({ errors: err });
		else res.json(categories);
	});
}

exports.getCategory = function(req, res){
	helpers.getCategory(parseInt(req.params.id), function(err, category){
		if(err) res.json({ errors: err });
		else res.json(category);
	})
}

exports.postCategory = function(req, res){
	helpers.postCategory({
		name: req.body['name'],
		description: req.body['description']
	}, function(err, category){
		if(err) res.json({ errors: err });
		else res.json(category);
	});
}

exports.putCategory = function(req, res){
	helpers.putCategory(req.params.id, {
		name		: req.params['name'],
		description	: req.params['description']
	}, function(err, category){
		if(err) res.json({ errors: err });
		else res.json(category);
	});
}

/**************************/

exports.getDiscussions = function(req, res){
	helpers.getDiscussions(parseInt(req.params.categoryId), function(err, discussions){
		if(err) res.json({ errors: err });
		else res.json(discussions);
	})
}

exports.getDiscussion = function(req, res){
	helpers.getDiscussion(parseInt(req.params.id), function(err, discussion){
		if(err) res.json({ errors: err });
		else res.json(discussion);
	});
}

exports.postDiscussion = function(req, res){
	console.log(req.session.userId);
	helpers.postDiscussion({
		categoryId	: req.body['categoryId'],
		name		: req.body['name'],
		body		: req.body['body'],
		announce	: req.body['announce'],
		userId		: req.session.userId
	}, function(err, discussion){
		if(err) res.json({ errors: err });
		else {
			sio.broadcast('discussion:new', discussion);
			res.json(discussion);
		}
	});
}

exports.putDiscussion = function(req, res){}

/**************************/

exports.getComments = function(req, res){
	helpers.getComments(parseInt(req.params.id), function(err, comments, discussion){
		if(err) res.json({ errors: err });
		else res.json({ discussion: discussion, comments: comments });
	});
}

exports.postComment = function(req, res){
	helpers.postComment({
		userId			: req.session.userId,
		discussionId	: req.body['discussionId'],
		body			: req.body['body']
	}, function(err, comment){
		if(err) res.json({ errors: err });
		else {
			sio.broadcast('comment:new', comment);
			res.json(comment);
		}
	});
}

/**************************/

exports.getActivities = function(req, res){
	helpers.getActivities(function(err, activities){
		if(err) res.json({ errors: err });
		else res.json( activities );
	})
}

exports.postActivity = function(req, res){
	helpers.postActivity({
		UserId	:	req.session.userId,
		username:	req.session.username,
		body	:	req.body['body']
	}, function(err, activity){
		if(err) res.json({ errors: err });
		else {
			sio.broadcast('activity:new', activity);
			res.json(activity);
		}
	});
}