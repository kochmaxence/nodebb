
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'nodeBB.xk', user: req.session });
};

exports.login = function(req, res){
	res.render('login', { title: 'nodeBB.xk : login'});
}

// exports.sync = function(req, res){
// 	models.Discussion.sync();
// 	models.Category.sync();
// 	models.Comment.sync();
// 	models.User.sync();
// 	models.Activity.sync();
// }