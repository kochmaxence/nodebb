var sio = require('socket.io').listen(3001);

var connections = {};

sio.sockets.on('connection', function(socket){
	console.log('new socket');
});

function broadcast(message, data){
	sio.sockets.emit(message, data);
}
exports.broadcast = broadcast;
